#include<stdio.h>
#include<string.h>
//creates the student structure
struct student{
	
	char name[20];
	char subject[30];
	float marks;
	
	};
	
int main()
{
		int i;
		int total;
		
		printf("Enter number of students : ");
		scanf("%d",&total);
		
		struct student array[total];
	//get the student data	
		for(i=0;i<total;i++)	
		{
		printf("Enter first name: ");
		scanf("%s",array[i].name);
		printf("Enter subject: ");
		scanf("%s",array[i].subject);
		printf("Enter marks: ");
		scanf("%f",&array[i].marks);
		printf("\n");
		}
		
		printf("\n Student details : \n");
		
		//printing gathered data
		for(i=0;i<total;i++)
		{
		printf("\t student name: %s\n", array[i].name);
		printf("\t subject: %s\n", array[i].subject);
		printf("\t marks: %.2f\n\n", array[i].marks);
		
		}
		
		return 0;
	
}


